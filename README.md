# LazyWorker

LazyWorker is a python script that keeps your terminal window busy while its just
scrolling down a file line per line at random pauses.
It looks like you are very busy but you are not ;).

## Getting started

Install Python

``` shell
sudo apt-get install python
```

Start the Script

``` shell
python LazyWork.py
```

done.

If you want to customize the output, replace the content in test.txt with your
content.
